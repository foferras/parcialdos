rm(list=ls())
setwd('C:/Users/if686806/Desktop/Parcial2/Modelos-de-Credito-Primavera2016/exam2')



Cap<- read.csv('stocks_exam.csv',header = T, stringsAsFactors = FALSE)
Cap<- Cap[,-1]

datos<- read.csv('liab_divs_exam.csv',header = T, stringsAsFactors = FALSE)
rownames(datos) <- datos$variables

obligaciones <- as.numeric(datos["TotalLiabilities", -1])
act.obligaciones <- as.numeric(datos["CurrentLiabilities", -1])
long.obligaciones <- obligaciones - act.obligaciones
ultimo <- nrow(Cap)
dividendosj<-  as.numeric(datos["Dividends", -1])

G <- as.numeric(datos["DividendsGrowth", -1])
C <- 0.03
R <- 0.01

x <- seq(from = 1, to = nrow(Cap), by = round(nrow(Cap)/6))
probabilidad <- data.frame(matrix(data=NA, nrow = length(x)-1, ncol = ncol(Cap)))

p <- c()
for(i in 1:500){
  L <- obligaciones[i]
  CL <- act.obligaciones[i]
  LTL <- L -CL
  tiempo <- (0.5*CL + 10 * LTL)/L
  D_0 <- dividendosj[i]
  g <- G[i]
  c <- 0.03
  r <- 0.01

  aux <- 0

      for(j in seq(from = 1, to = nrow(Cap), by = round(nrow(Cap)/6))){
    aumento <- round(nrow(Cap)/6)
    aux <- aux + 1 
    if(aux>6) next;
    if(Cap == x[length(x)]){aumento <- nrow(Cap)-j}
    E <- Cap[j:(j+aumento), i]
    D <- 0
    I <- 0
    for (k in 1:floor(tiempo)){
      D <- D + D_0 * (1 + g)^k * exp(r *( tiempo - k))
      I <- I + L * c * exp(r *( tiempo - k))
    }
    ndays <- 252
    
    A <- E + L
    
    sigma_E <- sd(diff(log(E)))*sqrt(ndays)
    sigma_A <- sd(diff(log(A)))*sqrt(ndays)
    
    d_1 <- (log(A/(L + D + I)) + (r + sigma_A^2)*(ndays))/(sigma_A)
    d_2 <- d_1 - sigma_A
    
    k_1 <- (log(A/(I + D))+(r + sigma_A^2/2))/(sigma_A)
    k_2 <- k_1 - sigma_A
    
    phi <- function(x) pnorm(x)
    A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
    ct <- 0
    while (sum((A-A_n)^2) > 0.007){
      sigma_A_n <- sd(diff(log(A_n)))*sqrt(ndays)
      
      d_1 <- (log(A_n/(L + I + D))+(r + sigma_A_n^2/2))/(sigma_A_n)
      d_2 <- d_1 - sigma_A_n
      
      k_1 <- (log(A_n/(I + D)) + (r + sigma_A_n^2/2))/(sigma_A_n)
      k_2 <- k_1 - sigma_A_n
      
      A <- A_n
      A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
      if(any(is.na(A_n)) || any(is.nan(A_n)) == T || any(A_n) <= 0) break;
      if (ct > 1000) break;
      ct <- ct + 1
    }

        ndays <- tiempo * 262
    
    sigma_A <- (sd(diff(log(A)))*sqrt(ndays))
    
    mu_A <- (mean(diff(log(A)))*ndays)
    
    lA_T <- rnorm(100000,log(A_n[length(A_n)]) - ( mu_A - sigma_A^2/2),sigma_A)
    
    p[aux] <- length(lA_T[lA_T < log(L + D + I)])/ length(lA_T)
    
  } 
  
  probabilidad[, i] <- 1 - (1-p)^(1/tiempo)  
  i=i+1
} 
probabilidad <- probabilidad[colSums(!is.na(probabilidad)) > 0]


prob.default <- probabilidad


#INCISO B

prob <- as.data.frame(prob.default)
limits <- c(0.0018, 0.0028, 0.01, 0.0211, 0.0822, 0.1853, 1)
nota <- c('AA', 'A', 'BBB', 'BB', 'B', 'CCC')


prob[prob < limits[1]] <- 'AAA'
prob[prob == 1]        <- 'Default'


for(index in 1:(length(limits)-1)){
  
  prob[ prob >= limits[index] & prob < limits[index+1] ] <- nota[index]
  
}


for(index in 1:ncol(prob)){
  if(sum(prob[, index] == 'Default') >= 1 ){
    posicion <- which(prob[, index] == 'Default')
    prob[min(posicion):nrow(prob), index] <- 'Default'
  }
}

prob

#INCISO C
prob <- t(prob)
pair_reloaded <- function(line, initial_class, given_class){
  a <- prob[line, -ncol(prob)]  # Ultimo vector
  b <- prob[line, -1]             # Primer vector
  count <- length(intersect(which(a == given_class), which(b == initial_class)))
  return(count)
}

nota <- c('AAA','AA','A','BBB','BB','B','CCC','Default')

total <- numeric(length(nota))

names(total) <- as.character(nota)

for(index in 1:length(nota)){
  
  total[index] <- sum(prob[, -ncol(prob)] == nota[index])
  
}

combinations <- merge(nota, nota)

prob.noigual <-  numeric(nrow(combinations)) 
names <- numeric()

for(index in 1:nrow(combinations)){
  initial_class <- as.character.factor(combinations[index, 1])
  given_class   <- as.character.factor(combinations[index, 2])
  names[index]  <- paste(initial_class, given_class, sep = ' | ')
  
  for(line in 1:nrow(prob)){
    
    prob.noigual[index] <- prob.noigual[index] + pair_reloaded(line,
                                                                   initial_class,
                                                                   given_class)
  }
  
}

names(prob.noigual) <- names

m.transicion<- data.frame(matrix(NA, nrow = length(nota), ncol = length(nota)))
colnames(m.transicion) <- as.character(nota)
rownames(m.transicion) <- as.character(nota)

for(index in 1:length(prob.noigual)){
  initial_class <- as.character.factor(combinations[index, 1])
  given_class   <- as.character.factor(combinations[index, 2])
  names  <- paste(initial_class, given_class, sep = ' | ')
  
  m.transicion[given_class, initial_class] <- prob.noigual[names] / total[given_class]
  
}